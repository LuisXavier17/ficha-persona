import {LitElement, html, css} from 'lit';

class PersonaForm extends LitElement {
  static get properties() {
    return {
      person: { type: Object }
    };
  }

  constructor() {
    super();
    this.person = {};
  }

  goBack(e) {
    console.log("goBack");
    e.preventDefault();
    this.dispatchEvent(new CustomEvent("persona-form-close", {}));
  }

  storePerson(e) {
    console.log("storePerson");
    e.preventDefault();

    this.person.photo = {
      "src": "./img/persona.jpg",
      "alt": "Persona"
    };

    console.log("La propiedad name vale " + this.person.name);
    console.log("La propiedad profile vale " + this.person.profile);
    console.log("La propiedad yearsInCompany vale " + this.person.yearsInCompany);

    this.dispatchEvent(new CustomEvent("persona-form-store", {
      detail: {
        person: {
          name: this.person.name,
          profile: this.person.profile,
          yearsInCompany: this.person.yearsInCompany,
          photo: this.person.photo
        }
      }
    })
    );
  }

  updateName(e) {
    console.log("updateName");
    console.log("Actualizando la propiedad name con el valor " + e.target.value);
    this.person.name = e.target.value;
  }

  updateProfile(e) {
    console.log("updateProfile");
    console.log("Actualizando la propiedad profile con el valor " + e.target.value);
    this.person.profile = e.target.value;
  }

  updateYearsInCompany(e) {
    console.log("updateYearsInCompany");
    console.log("Actualizando la propiedad yearsInCompany con el valor " + e.target.value);
    this.person.yearsInCompany = e.target.value;
  }


  render() {
    return html `
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <div>
      <form action="#">
        <div class="form-group">
          <label for="name">Nombre</label>
          <input type="text" id="name" class="form-control" placeholder="Nombre completo" @input="${this.updateName}"/>
        </div>
        <div class="form-group">
          <label for="profile">Perfil</label>
          <textarea id="profiel" rows="5" class="form-control" placeholder="Perfil" @input="${this.updateProfile}"></textarea>
        </div>
        <div class="form-group">
          <label for="years">Años en la empresa</label>
          <input id="years" type="text" class="form-control" placeholder="Años en la empresa" @input="${this.updateYearsInCompany}"/>
        </div>
        <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
        <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
      </form>
    </div>
    `;
  }
}

customElements.define('persona-form', PersonaForm);